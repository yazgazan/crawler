#!/usr/bin/env zsh

alias dum='du -h --max-depth=1'
alias grep='grep --color=always -ni'

while ((1))  {
  (
    echo -n "db : "
    ls db | wc -l
    dum db | cut -f 1
    echo -n "images : "
    ls images | wc -l
    dum images | cut -f 1
    echo -n "jobs : "
    ps aux | grep "phantomjs" | grep -v 'grep --color=always' | wc -l
  ) > /tmp/monitorbuff
  clear
  cat /tmp/monitorbuff
  sleep 1
}

