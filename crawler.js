
var page = require('webpage').create(),
    system = require('system'),
    fs = require('fs'),
    t, address, requests = [], responses = [],
    originalHost;

var saveDir = "/data/yazou/crawler/db";
var renderDir = '/data/yazou/crawler/images';
var debug = true;

var hostToPath = function hostToPath(host)  {
  return (saveDir + '/' + host + '.json');
};

var hostToImagePath = function hostToImagePath(host)  {
  return (renderDir + '/' + host + '.png');
};

var hostToUrl = function hostToUrl(host)  {
  return ('http://' + host + '/');
};

if (system.args.length <= 2) {
  console.log('Usage: crawler.js <some URL>');
  phantom.exit();
}

page.onResourceRequested = function (request) {
  requests.push(request);
};
page.onResourceReceived = function (response) {
  responses.push(response);
};

// setTimeout(function ()  {
//     phantom.exit();
//     }, 1000 * 60);

t = Date.now();
address = system.args[1];
originalHost = system.args[2];
page.open(address, function (status) {
  var ret;
  var retStr;

  if (status !== 'success') {
    ret = {
      status: 'fail'
    };
  } else {
    t = Date.now() - t;
    ret = page.evaluate(function ()  {
      var ret = {};
      var links;
      var scripts;
      var images;

      ret.title = document.title;
      ret.url = document.location.href;
      ret.html = document.body.parentElement.outerHTML;
      ret.text = document.body.parentElement.outerText;
      ret.host = document.location.host;
      links = document.getElementsByTagName('a');
      scripts = document.getElementsByTagName('script');
      images = document.getElementsByTagName('img');
      ret.status = 'success';
      ret.links = [];
      ret.externalsLinks = [];
      ret.externalsHosts = [];
      ret.scripts = [];
      ret.images = [];
      for (id in links)  {
        if (links[id].href)  {
          if (links[id].host !== document.location.host)  {
            ret.externalsLinks.push(links[id].href);
            ret.externalsHosts.push(links[id].host);
          }
          ret.links.push(links[id].href);
        }
      }
      for (id in scripts)  {
        if (scripts[id].src)  {
          ret.scripts.push(scripts[id].src);
        }
      }
      for (id in images)  {
        if (images[id].src)  {
          ret.images.push({
            src: images[id].src,
            name: images[id].name,
            alt: images[id].alt
          });
        }
      }
      ret.jsVars = [];
      for (id in window)  {
        ret.jsVars.push(id);
      }
      return ret;
    });
    ret.loadTime = t;
    ret.requests = requests;
    ret.responses = responses;
  }
  if (debug)
    retStr = JSON.stringify(ret, null, 2);
  else
    retStr = JSON.stringify(ret);
  if (ret.status === 'success')  {
    page.render(hostToImagePath(originalHost));
  }
  try  {
    fs.write(hostToPath(originalHost), retStr, 'w');
  } catch (e)  { }
  phantom.exit();
});

