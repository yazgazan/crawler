
var fs = require('fs');

var dbdir = process.env['DBDIR'] || 'db/';
var queueFile = process.env['QUEUE_FILE'] || 'download.queue';
var debug = process.env['DEBUG'] !== undefined || false;

var downloadQueue = new Array;

var treatSite = function treatSite(site)  {
  var hosts = site.externalsHosts;

  for (var i in hosts)  {
    var host = hosts[i];

    if (downloadQueue.indexOf(host) !== -1)
      continue;
    try  {
      var stats = fs.statSync(dbdir + '/' + host + '.json');
    } catch (e)  {
      downloadQueue.push(host);
    }
  }
};

fs.readdir(dbdir, function (err, files)  {
  var count = 0;
  var prev = -1;

  for (var i in files)  {
    var fileName = files[i];
    var fullPath = dbdir + '/' + fileName;

    try  {
      var site = JSON.parse(fs.readFileSync(fullPath));

      treatSite(site);
    } catch (e)  { }
    ++count;
    var percents = Math.round(count / files.length * 100);
    if (percents != prev)  {
      console.log(percents + '% (' + downloadQueue.length + ')');
    }
    prev = percents;
  }
  console.log('writing downloadQueue to disque (' + downloadQueue.length + ')');
  if (debug)  {
    fs.writeFileSync(queueFile, JSON.stringify(downloadQueue, null, 2));
  }
  else  {
    fs.writeFileSync(queueFile, JSON.stringify(downloadQueue));
  }
});

