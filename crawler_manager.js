
require('sugar');
var express = require('express');
var fs = require('fs');
var exec = require('child_process').exec;
var app = express();

var port = +process.env['PORT'] || 3000;
var queueSize;
if (process.env['QUEUE_SIZE'] !== undefined)  {
  queueSize = +process.env['QUEUE_SIZE'];
}
else  {
  queueSize = 10;
}
var timeout = +process.env['TIMEOUT'] || 60;
var writeTimeout = +process.env['WRITE_TIMEOUT'] || 10;

var downloadQueue = [];
var childrenCount = 0;

var saveDir = "/data/yazou/crawler/db";
var downloadQueueFile = __dirname + '/download.queue';
var renderDir = '/data/yazou/crawler/images';

try  {
  downloadQueue = JSON.parse(fs.readFileSync(downloadQueueFile)).unique();
  console.log('download Queue loaded');
}
catch (e)  {
  downloadQueue = [];
}

var hostToPath = function hostToPath(host)  {
  return (saveDir + '/' + host + '.json');
};

var hostToImagePath = function hostToImagePath(host)  {
  return (renderDir + '/' + host + '.png');
};

var hostToUrl = function hostToUrl(host)  {
  return ('http://' + host + '/');
};

var addHostToQueue = function addHostToQueue(host, cb, front)  {
  var path = hostToPath(host);

  if (front === undefined)
    front = false;
  if (downloadQueue.indexOf(host) === -1 || front)  {
    fs.stat(path, function (err, stats)  {
      if (!err) return;
      if (front)
        downloadQueue.unshift(host);
      else
        downloadQueue.push(host);
      if (cb !== undefined && (typeof cb) === 'function')  {
        cb();
      }
    });
  }
};

app.get('/', function (req, res)  {
  res.redirect('status');
});

app.get('/status', function (req, res)  {
    res.send({
      sizeQueue: downloadQueue.length,
      childrenCount: childrenCount
    });
});

app.get('/threads/set/:nthreads', function (req, res)  {
  var nThreads = req.params.nthreads;

  queueSize = nThreads;
  res.send(200, {
    status: "ok",
    nThreads: queueSize
  });
});

app.get('/timeout/set/:timeout', function (req, res)  {
  timeout = +req.params.timeout;

  res.send(200, {
    status: "ok",
    timeout: timeout
  });
});

app.get('/:host', function(req, res){
  var host = req.params.host;

  addHostToQueue(host, function ()  { crawl(true); }, true);
  fs.readFile(hostToPath(host), function (err, data)  {
    if (err)  {
      var idInQueue = downloadQueue.indexOf(host);

      res.send(404, {
        status: "not found",
        idInQueue: idInQueue
      });
      return;
    }
    res.set({
      'Content-Type': 'text/plain',
      'Content-Length': data.length
    });
    res.send(data);
  });
});

app.get('/:host/image', function(req, res){
  var host = req.params.host;

  addHostToQueue(host, function ()  { crawl(true); }, true);
  fs.stat(hostToImagePath(host), function (err, stats)  {
    if (err)  {
      var idInQueue = downloadQueue.indexOf(host);

      res.send(404, {
        status: "not found",
        idInQueue: idInQueue
      });
      return;
    }
    res.attachment(hostToImagePath(host));
    res.sendfile(hostToImagePath(host));
  });
});

app.get('/:host/image/view', function(req, res){
  var host = req.params.host;

  addHostToQueue(host, function ()  { crawl(true); }, true);
  fs.stat(hostToImagePath(host), function (err, stats)  {
    if (err)  {
      var idInQueue = downloadQueue.indexOf(host);

      res.send(404, {
        status: "not found",
        idInQueue: idInQueue
      });
      return;
    }
    res.sendfile(hostToImagePath(host));
  });
});

app.listen(port);

var saveDownloadQueue = function saveDownloadQueue()  {
  try  {
    fs.renameSync(downloadQueueFile, downloadQueueFile + '.backup');
    fs.writeFile(downloadQueueFile, JSON.stringify(downloadQueue));
  } catch (e) {}
};

var crawl = function crawl(dontCareThreadLimits)  {
  if (!dontCareThreadLimits)  {
    if (childrenCount >= queueSize)
      return;
    if (downloadQueue.length === 0)
      return;
  }

  var site = downloadQueue.shift();
  console.log('crawling "' + hostToUrl(site) + '"');
  var child = exec('phantomjs crawler.js "' + hostToUrl(site) + '" "' + site + '"',
      {timeout: timeout * 1000},
      function (error, stdout, stderr)  {
        --childrenCount;
        if (error) return;
        fs.readFile(hostToPath(site), function (err, data)  {
          var result;

          if (err) return;
          try  {
            result = JSON.parse(data);
          } catch (e)  {
            return;
          }
          if (result.status !== 'success') return;
          if (result.externalsHosts === undefined) return;
          result.externalsHosts.map(function (item)  {
            addHostToQueue(item, crawl);
          });
          crawl();
        });
      });
  childrenCount++;
  crawl();
};

// for (var i = 2; i < process.argv.length; ++i)  {
//   addHostToQueue(process.argv[i], crawl);
// }

setInterval(function ()  {
  crawl();
  // downloadQueue = downloadQueue.unique();
}, 1000);

setInterval(function ()  {
    saveDownloadQueue();
}, writeTimeout * 1000);

